+++
title="1-Local To North Carolina"
description = "This is things that is Though out North Carolina"
+++
## Hickory 
- [PFLAG](https://www.pflaghickorync.com/) Hickory offers support and resources for LGBTQ+ individuals and their families in Hickory, North Carolina. 
- [Catawba Valley Pride](https://www.facebook.com/CatawbaValleyPride) is a Hickory-based Facebook page promoting LGBTQ+ awareness and events in the Catawba Valley area.
## Ashevile 
- [Tranzmission](https://tranzmission.org/) in Asheville provides resources and support for the transgender and gender non-conforming community. 
- [The Hive 828]( https://www.thehive828.org/tgnc-health-resources) in Asheville offers transgender and gender non-conforming health resources and support  
- [Youth OUTright](https://www.youthoutright.org/) is an organization in Asheville dedicated to empowering LGBTQ+ youth. 
- [Blue Ridge Pride](https://blueridgepride.org/) organizes LGBTQ+ pride events and advocacy efforts in the Asheville region  
- [Phoenix Transgender Support](https://phoenixtgs.weebly.com/index.html) provides resources and information for transgender individuals in Asheville. 
- [WNCCHS](https://www.wncchs.org/trans-health)Trans Health offers healthcare and support for transgender individuals in Asheville and surrounding counties. 
## Charlotte 
- [Transcend Charlotte](https://www.transcendcharlotte.org/) is a resource center and support network for transgender individuals in Charlotte.
- [Charlotte Transgender Healthcare](https://www.charlottetranshealth.org/) Network provides healthcare and support services for transgender individuals in Charlotte.
- [Carolina Transgender Society](https://www.carolinatransgendersociety.com/) is an organization in Charlotte supporting the transgender community.
- [Guilford Green Foundation](https://guilfordgreenfoundation.org/) Foundation in Greensboro works to advance LGBTQ+ causes in the community.
- [PFLAG](https://www.pflaggreensboro.org/) Greensboro offers support and advocacy for LGBTQ+ individuals and their families in Greensboro.
## Raleigh
- [LGBTQ Center of Durham](https://www.lgbtqcenterofdurham.org/transgender-and-gender-affirming-resources/) provides transgender and gender-affirming resources and support in Durham. 
- [LGBTQ Center of Raleigh](https://www.lgbtcenterofraleigh.com/initiatives/transgender-programs.html) offers transgender programs and resources in Raleigh.





