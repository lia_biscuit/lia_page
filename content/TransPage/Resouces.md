+++
title="0-Resources"
description = "This is stuff that can be found oninle or nation wide. You can aso give input on this page to help me expand this list"
+++
- **Southern Equality**
    **Website:** [Southern Equality](https://southernequality.org)
   - **Description:** Southern Equality is an organization dedicated to advancing LGBTQ+ rights and equality in the southern United States. They provide resources, support, and advocacy to empower LGBTQ+ individuals and communities, with a focus on addressing the unique challenges faced in the southern region of the country.

- **Plume**
   - **Website:** [Plume](https://getplume.co)
   - **Description:** Plume is a telehealth platform specializing in gender-affirming hormone therapy. They offer personalized care for transgender and non-binary individuals seeking hormone therapy, making it more accessible and convenient to access gender-affirming care.

- **Folx Health**
   - **Website:** [Folx Health](https://www.folxhealth.com)
   - **Description:** Folx Health is a digital healthcare platform designed to provide comprehensive and inclusive healthcare services for the LGBTQ+ community. They offer a range of services, including hormone therapy, sexual health care, and mental health support, with a focus on LGBTQ+ individuals' specific needs and experiences.

- **The Gender Dysphoria Bible**
   - **Website:** [The Gender Dysphoria Bible](https://genderdysphoria.fyi/en)
   - **Description:** The Gender Dysphoria Bible is an online resource that provides information and support for individuals dealing with gender dysphoria. It offers educational content, personal stories, and resources to help those navigating the challenges associated with gender dysphoria, aiming to foster understanding and empathy within and outside the transgender community.

- **LGBT Friendly Thereapists**
   - **Website:**:[LGBT Friendly Thereapists](https://www.inclusivetherapists.com/about)
   - **Description:** At Inclusive Therapists, we understand the importance of finding a safe and affirming space for your mental health and well-being. Our platform connects you with a diverse network of therapists who are not only professionally qualified but also deeply committed to providing compassionate and inclusive support to individuals across the LGBTQ+ spectrum. Here, you'll find a community that celebrates your identity and respects your unique journey. Explore our network of LGBTQ+ friendly therapists and take the first step towards your path to healing and self-discovery.



<h4 style="color: #fff; font-size: 20px; margin-bottom: 10px;">Help Me expand by placing your comments here:</h4>
<div id="cusdis_thread" data-host="https://cusdis.com"
  data-app-id="a06a5450-3c9f-4a7b-8e32-fc9cc8df1bb8"
  data-theme = "dark"
  data-page-id="{{ .File.UniqueID }}" data-page-url="{{ .Permalink }}"
  data-page-title="{{ .title }}">
</div>
<script async defer src="https://cusdis.com/js/cusdis.es.js"></script>
