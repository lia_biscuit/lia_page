+++
title="2-Appstate Resoures"
description = "This is things that you can find here at appstate"
+++

- [Counseling and Psychological Resources](https://counseling.appstate.edu/ )
- [Henderson Springs LGBT Center](https://lgbt.appstate.edu/)
- [M. S. Shook Student Health Services](https://healthservices.appstate.edu/ )
- [Women's Center](https://womenscenter.appstate.edu/ )
- [Office of Title IX Compliance](https://titleix.appstate.edu/) 
- [Student Legal Clinic](https://legalclinic.appstate.edu/)
- [University Ombuds Office](https://ombuds.appstate.edu/ )
- [Gender Accommodations for On Campus Housing](https://housing.appstate.edu/accommodations#:~:text=We%20offer%20some%20gender%2Dinclusive,gender%20expression%20is%20different%20from)
- [Preferred First Name Initiative](https://diversity.appstate.edu/preferred-first-name/ )
 